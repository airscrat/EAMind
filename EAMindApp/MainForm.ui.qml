import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2
import QtQuick.Controls.Styles 1.4

Item {
    width: 640
    height: 480

    property alias button1: button1
    property alias button2: button2

    RowLayout {
        width: 221
        height: 23
        anchors.verticalCenterOffset: -48
        anchors.horizontalCenterOffset: 129
        anchors.centerIn: parent

        Button {
            id: button1
            text: qsTr("Press Me 1")
            style: buttonStyle
        }

        Button {
            id: button2
            text: qsTr("Press Me 2")
            style: buttonStyle
        }
    }

    Button {
        id: button3
        objectName: buttonCplus
        x: 338
        y: 260
        text: qsTr("中文测试")
        width:160
        height:30
        style: buttonStyle
    }

    Button {
        id: button
        x: 338
        y: 128
        text: qsTr("Button")
    }

    Calendar {
        id: calendar
        x: 33
        y: 15
    }

    CheckBox {
        id: checkBox
        x: 434
        y: 132
        text: qsTr("Check Box")
    }

    ComboBox {
        id: comboBox
        x: 338
        y: 88
    }

    Slider {
        id: sliderHorizontal
        x: 338
        y: 224
    }

    GridView {
        id: gridView
        x: 48
        y: 287
        width: 140
        height: 140
        model: ListModel {
            ListElement {
                name: "Grey"
                colorCode: "grey"
            }

            ListElement {
                name: "Red"
                colorCode: "red"
            }

            ListElement {
                name: "Blue"
                colorCode: "blue"
            }

            ListElement {
                name: "Green"
                colorCode: "green"
            }
        }
        cellWidth: 70
        cellHeight: 70
        delegate: Item {
            x: 5
            height: 50
            Column {
                spacing: 5
                Rectangle {
                    width: 40
                    height: 40
                    color: colorCode
                    anchors.horizontalCenter: parent.horizontalCenter
                }

                Text {
                    x: 5
                    text: name
                    anchors.horizontalCenter: parent.horizontalCenter
                    font.bold: true
                }
            }
        }
    }

    Connections {
        target: button3
        onClicked: print("clicked")
    }

    RadioButton {
        id: radioButton
        x: 338
        y: 311
        text: qsTr("Radio Button")
    }

    ProgressBar {
        id: progressBar
        x: 338
        y: 346
    }
}
