import QtQuick 2.0
import QtQuick.Controls.Styles 1.4

Item {
    Component {
        id: buttonStyle
        ButtonStyle {
            background:Rectangle{
                radius:5
                border.width:2
                border.color:control.pressed? "#3399ff":"gray"
                color:control.pressed?"#cacfd2":"#3399ff"
            }
            label:Text{
                color: "white"
                text:control.text
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
            }
        }
    }
}
