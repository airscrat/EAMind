/********************************************************
[DateTime]:2017.08.20
[Author  ]:Mr.Huang
[Email   ]:airscrat(at)qq(dot)com
[Contents]:
/********************************************************/
#ifndef IPLUGINOBJECT_H
#define IPLUGINOBJECT_H

#include "eaengine_global.h"
#include <QtPlugin>
#include <QString>

namespace EAEngine {
    class IPluginObject
    {
    public:
        virtual ~IPluginObject(){}

        virtual const QString GetPluginUid()=0;
        virtual const int GetPluginIdx()=0;
        virtual const QString GetPluginText()=0;
        virtual void OnCreate()=0;
    };
}
#define IPluginObject_iid "EAEngine.IPluginObject"
Q_DECLARE_INTERFACE(EAEngine::IPluginObject, IPluginObject_iid)

#endif // IPLUGINOBJECT_H
