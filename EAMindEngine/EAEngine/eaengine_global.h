#ifndef EAENGINE_GLOBAL_H
#define EAENGINE_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(EAENGINE_LIBRARY)
#  define EAENGINESHARED_EXPORT Q_DECL_EXPORT
#else
#  define EAENGINESHARED_EXPORT Q_DECL_IMPORT
#endif

#ifdef Q_OS_WIN
#define EA_FUC_EXPORT __declspec(dllexport)
#else
#define EA_FUC_EXPORT
#endif

#endif // EAENGINE_GLOBAL_H
