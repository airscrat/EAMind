﻿/********************************************************
[DateTime]:2017.08.20
[Author  ]:Mr.Huang
[Email   ]:airscrat(at)qq(dot)com
[Content ]:
/********************************************************/
#ifndef PLUGINENGINE_H
#define PLUGINENGINE_H

#include "eaengine_global.h"
#include "IPluginObject.h"
#include <QPluginLoader>
#include <QVector>
#include <QDir>

namespace EAEngine {

    class EAENGINESHARED_EXPORT PluginEngine: public QObject
    {
        Q_OBJECT
    public:
        PluginEngine();
        ~PluginEngine();
        /**Get all plugins from library*/
        QVector<IPluginObject*>* GetPlugins();
    private:
        QVector<IPluginObject*>* plugins;
    };    
}

extern "C" Q_DECL_EXPORT QObject* GetInstance();
#endif // PLUGINENGINE_H
