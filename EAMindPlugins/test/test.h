﻿#ifndef TEST_H
#define TEST_H

//#include "test_global.h"
#include "IPluginObject.h"

using namespace EAEngine;
class Test: public QObject,public IPluginObject
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "test" FILE "test.json")
    Q_INTERFACES(EAEngine::IPluginObject)
public:
    Test();
    ~Test(){}
    const QString GetPluginUid(){return "{DC7A2AEE-0CAA-42A6-90B5-D5C5F097A84D}";}
    const int GetPluginIdx(){return 0;}
    const QString GetPluginText(){return "a test plugin";}
    void OnCreate(){}
private:

};

#endif // TEST_H
